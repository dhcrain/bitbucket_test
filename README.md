# Set Up Bitbucket

- Both people create an account (https://bitbucket.org/)
- One person create a repo
    - Go to settings, User and group access, add your partner as an admin
- Then in your project directory
    - (this is using https, not ssh, so it will ask for you password when you push, pull etc…)
    - `git remote add bucket <bitbucket_url_here>`
    - `git push -u bucket --all` *pushes up the repo and its refs for the first time*
- __NOW__ to use Bitbucket - you will need to:
    - `git push bucket master`
    - `git pull —rebase bucket master`

__Notice the used of *bucket* and *origin* for the remote name__  

[Dive into git remotes here](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes)

## When GitHub is back up...
You can go back to using:  

- `git push origin master`
- `git pull —rebase origin master`
